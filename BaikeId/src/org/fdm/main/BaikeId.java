package org.fdm.main;

import javax.annotation.Resource;

import org.fdm.baikeDao.BaikeIdDao;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class BaikeId {
	private BaikeIdDao personInforDao;
	
	public BaikeIdDao getPersonInforDao() {
		return personInforDao;
	}

	@Resource
	public void setPersonInforDao(BaikeIdDao personInforDao) {
		this.personInforDao = personInforDao;
	}

	public static void main(String[] args) {
long startTime=System.currentTimeMillis();   //获取开始时间  

		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("bean.xml");		
		BaikeId baikeId = (BaikeId)ctx.getBean("baikeId");
		baikeId.getPersonInforDao().getBaike();
		
long endTime=System.currentTimeMillis(); //获取结束时间  
System.out.println("程序运行时间： "+(endTime-startTime)+"ms"); 
	}
}
