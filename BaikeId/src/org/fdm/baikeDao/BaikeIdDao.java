package org.fdm.baikeDao;

import java.util.List;

import javax.annotation.Resource;

import org.fdm.baikeParser.BaikeBatch;
import org.fdm.tables.PersonInfor;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

@Component
public class BaikeIdDao {
	private SessionFactory sessionFactory;
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Resource
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public void getBaike(){
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		//set parameters for how many peoples in every thread, manually
		int begin = 1;
		int step = 5;
		//int max = 92033;
		int max = 100;
		for(int j=begin;j<=max;j=j+step){
			Query q = session.createQuery("from PersonInfor as u where u.id<"+String.valueOf(j+step)+"and u.id>="+String.valueOf(j) );
			List<PersonInfor> nameList = q.list();
			BaikeBatch batch = new BaikeBatch(nameList,sessionFactory);
			batch.start();
		}
		
		try{    
			Thread.sleep(1000);
		}catch(Exception e){}
		
		while(BaikeBatch.count!=0){
			try{    
				Thread.sleep(1000);
			}catch(Exception e){}
		}
		session.getTransaction().commit();
		session.close();
	}

}



