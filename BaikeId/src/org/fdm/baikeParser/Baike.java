package org.fdm.baikeParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Baike {
	private String name;
	private static Pattern baikeHttp = Pattern.compile("http://baike.baidu.com/view/\\d+\\.htm");;
	private Matcher urlMatcher;
	private URL baikeUrl;
	private int baikeId;
	
	public Baike(String inputName) throws IOException{
		if(filterChi(inputName)!="")
			name = filterChi(inputName);
		else
			name = inputName;
		request(name);
	}
	
	public URL getUrl(){
		return baikeUrl;
	}
	
	public String getName(){
		return name;
	}
	
	public int getId(){
		return baikeId;
	}
	
	private int request(String inputName) throws IOException{
		String line = "";
		StringBuffer baiduBuf = new StringBuffer();
		int idStart;
		int idEnd;
		
		try{
			URL baiduUrl = new URL("http://www.baidu.com/s?wd="+inputName+"&tn=sogouie_1_dg");
			InputStream baiduStream = baiduUrl.openStream();
			BufferedReader baiduReader = new BufferedReader(new InputStreamReader(baiduStream));
			while((line=baiduReader.readLine())!=null){
				baiduBuf.append(line);
			}
			String baike = getBaikeLink(baiduBuf.toString());
			baikeUrl = new URL(baike);
			idStart = baike.indexOf("view/");
			idEnd = baike.indexOf(".htm");
			baikeId = Integer.valueOf(baike.substring(idStart+5, idEnd));
			return 0; //without getting baike content
			//return getBaike(baike);
		}catch(MalformedURLException e){
			err("baiduUrl Error:",e);
		}
		
		return 0;
		
	}
	
	private String getBaikeLink(String Content){
		urlMatcher = baikeHttp.matcher(Content);
		if(urlMatcher.find())
			return urlMatcher.group();
		return "noValidLink";
	}
	
	
	private String filterChi(String name){
		String chi="";
		char c = ' ';
		for(int i=0;i<name.length();i++){
			c = name.charAt(i);
		    if ((c >= 0x4e00)&&(c <= 0x9fbb)){
		        chi = chi+c;
		    }
		}
		return chi;
	}
	
	private void err(String localInfo,Exception e){
		System.out.println(localInfo+e);
	}
	
	public static void main(String args[]) throws IOException{
		Baike test = new Baike("��.#*��.asdsd.��");
		System.out.println(test.getUrl());
		System.out.println(test.getId());
		System.out.println(test.getName());
	}
}
