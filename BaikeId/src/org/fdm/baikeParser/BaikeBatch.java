package org.fdm.baikeParser;

import java.io.IOException;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.fdm.tables.PersonId;
import org.fdm.tables.PersonInfor;

public class BaikeBatch extends Thread{
	public static int count=0;
	private List<PersonInfor> name;
	private SessionFactory sessionFactory;

	public BaikeBatch(List<PersonInfor> nameList,SessionFactory sessionFactory){
		name = nameList;
		this.sessionFactory = sessionFactory;
	}
	
	public void run(){
		count++;
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		for(int i=0;i<name.size();i++){
			try {
				Baike baike = new Baike(name.get(i).getName());
				PersonId pi = new PersonId();
				pi.setBid(baike.getId());
				pi.setName(baike.getName());
				session.save(pi);
			} catch (IOException e) {
				e.printStackTrace();
			}
			if(i % 50 == 0)
			{
				session.flush();
				session.clear();
			}
		}
		
		session.getTransaction().commit();
		session.close();
		count--;
	}
}
